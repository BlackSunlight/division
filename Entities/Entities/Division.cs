﻿using System.Collections.Generic;
using LogData;

namespace Entities
{
    public class Division
    {

        private List<AbstractWeapon> weapons = new List<AbstractWeapon>();
        public List<AbstractWeapon> Weapons
        {
            get { return weapons; }
        }
        public Division()
        {
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log("Division created", "division.log");
        }
        /// <summary>
        /// Adds weapon in division
        /// </summary>
        /// <param name="weapon">A weapon that will be added</param>
        public void AddWeapon(AbstractWeapon weapon)
        {
            this.weapons.Add(weapon);
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} added", weapon.Name),"division.log");  
        }

        /// <summary>
        /// Deletes weapon from division
        /// </summary>
        /// <param name="weapon">A weapon what will be removed</param>
        public void DeleteWeapon(AbstractWeapon weapon)
        {
            this.weapons.Remove(weapon);
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} removed", weapon.Name),"division.log");
        }
    }
}
