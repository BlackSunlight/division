﻿using LogData;

namespace Entities
{
    public class Knife : AbstractWeapon
    {
        private int _bladeLenght;
        public int BladeLenght
        {
            get { return _bladeLenght; }
        }

        public Knife(string name, int bladeLenght, double price, double weight) : base(name, price, weight)
        {
            this.Name = name;
            this.Price = price;
            this._bladeLenght = bladeLenght;
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} created", name), "weapons.log");
        }
    }
}
