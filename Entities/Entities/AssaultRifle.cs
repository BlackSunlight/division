﻿using LogData;

namespace Entities
{
    public class AssaultRifle : Gun
    {
        private int _rate;

        public int Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        public AssaultRifle(string name, double price, double weight, int rate, int capacity, double calibre) : base (name, price, weight, capacity, calibre)
        {
            this._rate = rate;
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} created", name), "weapons.log");
        }
    }
}
