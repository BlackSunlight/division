﻿using LogData;

namespace Entities
{
    public class Gun : AbstractWeapon
    {
        private int _capacity;
        private double _calibre;

        public int Capacity
        {
            get { return _capacity; }
            set { _capacity = value; }
        }

        public double Calibre
        {
            get { return _calibre; }
            set { _calibre = value; }
        }

        public Gun(string name, double price, double weight, int capacity, double calibre ) : base(name, price, weight)
        {
            this._calibre = calibre;
            this._capacity = capacity;
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} created", name), "weapons.log");
        }
    }
}
