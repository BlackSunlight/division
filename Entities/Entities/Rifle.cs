﻿using LogData;

namespace Entities
{
    public class Rifle : Gun
    {
        private int _range;
        private int _zoom;
        public int Range
        {
            get { return _range; }
        }
        
        public int Zoom
        {
            get { return _zoom; }
            set { _zoom = value; }
        }
        public Rifle (string name, double price, double weight, int range,int zoom,int capacity, double calibre) : base(name, price,weight,capacity,calibre)
        {
            this._range = range;
            this._zoom = zoom;
            var logger = LoggerFactory.GetLogger(LogType.File);
            logger.Log(string.Format("Weapon {0} created", name), "weapons.log");
        }
    }
}
