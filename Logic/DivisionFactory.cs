﻿using LogData;
using Entities;


namespace Logic
{
    public static class DivisionFactory
    {
        public static Division CreateDivision()
        {
            var division = new Division();

            var pistol = new Gun("Beretta", 12.5, 2.0, 10, 5.65);
            division.AddWeapon(pistol);

            var rifle = new Rifle("L96", 50, 10, 1000, 10, 10, 5.65);
            division.AddWeapon(rifle);

            var assaultRifle = new AssaultRifle("M16", 45, 7, 900, 30, 4.2);
            division.AddWeapon(assaultRifle);

            var knife = new Knife("Butterfly", 15, 5, 0.7);
            division.AddWeapon(knife);

            return division;
        }
    }
}
