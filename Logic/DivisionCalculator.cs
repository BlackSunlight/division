﻿using System.Collections.Generic;
using Entities;

namespace Logic
{
    public static class DivisionCalculator
    {

        public static double GetTotalPrice(Division division)
        {
            List<AbstractWeapon> weapons = division.Weapons;
            double totalPrice = 0;
            foreach (AbstractWeapon weapon in weapons)
            {
                totalPrice += weapon.Price;
            }
            return totalPrice;
        }

        public static double GetTotalWeight(Division division)
        {
            List<AbstractWeapon> weapons = division.Weapons;
            double totalWeight = 0;
            foreach (AbstractWeapon weapon in weapons)
            {
                totalWeight += weapon.Weight;
            }
            return totalWeight;
        }
    }
}
