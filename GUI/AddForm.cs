﻿using System;
using System.Windows.Forms;
using Entities;

namespace GUI
{
    public partial class AddForm : Form
    {
        AbstractWeapon _weapon = null;
        public AddForm()
        {
            InitializeComponent();
            SetInvisibleAllTxtBox();
            okBtn.Enabled = false;
        }

        public AddForm(AbstractWeapon weapon)
        {
            InitializeComponent();
            okBtn.Visible = false;
            editBtn.Visible = true;
            comboBoxClass.Enabled = false;
            _weapon = weapon;
            this.Refresh();
            FillFields();
        }

        private void FillFields()
        {
           // MessageBox.Show(_weapon.GetType().ToString(),"_weapon");
            SetVisibleAllTxtBox();
            if (_weapon.GetType() == typeof(Knife))
            {
                Knife weap = (Knife)_weapon;
                textBoxCalibre.Visible = false;
                textBoxCapacity.Visible = false;
                textBoxRange.Visible = false;
                comboBoxZoom.Visible = false;
                comboBoxZoom.Visible = false;
                textBoxRate.Visible = false;
                comboBoxClass.SelectedValue = "Knife";
                textBoxBlade.Enabled = false;

                textBoxName.Text = weap.Name;
                textBoxWeight.Text = weap.Weight.ToString();
                textBoxPrice.Text = weap.Price.ToString();
                textBoxBlade.Text = weap.BladeLenght.ToString();
            }
            if (_weapon.GetType() == typeof(Gun))
            {
                Gun weap = (Gun)_weapon;
                textBoxRange.Visible = false;
                comboBoxZoom.Visible = false;
                textBoxBlade.Visible = false;
                textBoxRate.Visible = false;
                comboBoxClass.SelectedValue = "Gun";

                textBoxName.Text = weap.Name;
                textBoxWeight.Text = weap.Weight.ToString();
                textBoxPrice.Text = weap.Price.ToString();
                textBoxCalibre.Text = weap.Calibre.ToString();
                textBoxCapacity.Text = weap.Capacity.ToString();
            }
            if (_weapon.GetType() == typeof(AssaultRifle))
            {
                AssaultRifle weap = (AssaultRifle)_weapon;

                comboBoxZoom.Visible = false;
                textBoxBlade.Visible = false;
                textBoxRange.Visible = false;
                comboBoxClass.SelectedValue = "AssaultRifle";

                textBoxName.Text = weap.Name;
                textBoxWeight.Text = weap.Weight.ToString();
                textBoxPrice.Text = weap.Price.ToString();
                textBoxCalibre.Text = weap.Calibre.ToString();
                textBoxCapacity.Text = weap.Capacity.ToString();
                textBoxRate.Text = weap.Rate.ToString();
            }
            if (_weapon.GetType() == typeof(Rifle))
            {
                Rifle weap = (Rifle)_weapon;

                textBoxBlade.Visible = false;
                textBoxRate.Visible = false;
                comboBoxClass.SelectedValue = "Rifle";
                textBoxRange.Enabled = false;

                textBoxName.Text = weap.Name;
                textBoxWeight.Text = weap.Weight.ToString();
                textBoxPrice.Text = weap.Price.ToString();
                textBoxCalibre.Text = weap.Calibre.ToString();
                textBoxCapacity.Text = weap.Capacity.ToString();
                textBoxRange.Text = weap.Range.ToString();
                comboBoxZoom.SelectedItem = weap.Zoom.ToString();
            }

        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            switch (comboBoxClass.SelectedItem.ToString())
            {
                case "Gun":
                    {
                        if ((textBoxName.Text == "")||(textBoxWeight.Text=="")||(textBoxCalibre.Text=="")||(textBoxCapacity.Text=="")||(textBoxPrice.Text==""))
                        {
                            MessageBox.Show("All fields must be filled!");
                        }
                        else
                        {
                            double price = Convert.ToDouble(textBoxPrice.Text);
                            double weight = Convert.ToDouble(textBoxWeight.Text);
                            int capacity = Convert.ToInt32(textBoxCapacity.Text);
                            double calibre = Convert.ToDouble(textBoxCalibre.Text);
                            _weapon = new Gun(textBoxName.Text, price, weight, capacity, calibre);
                            Data.Division.AddWeapon(_weapon);
                            this.Close();
                        }
                    }
                    break;

                case "AssaultRifle":
                    {
                        if ((textBoxName.Text == "") || (textBoxWeight.Text == "") || (textBoxCalibre.Text == "") || (textBoxCapacity.Text == "")
                             || (textBoxPrice.Text == "") || (textBoxRate.Text == ""))
                        {
                            MessageBox.Show("All fields must be filled!");
                        }
                        else
                        {
                            double price = Convert.ToDouble(textBoxPrice.Text);
                            double weight = Convert.ToDouble(textBoxWeight.Text);
                            int capacity = Convert.ToInt32(textBoxCapacity.Text);
                            double calibre = Convert.ToDouble(textBoxCalibre.Text);
                            int rate = Convert.ToInt32(textBoxRate.Text);
                            _weapon = new AssaultRifle(textBoxName.Text, price, weight, rate, capacity, calibre);
                            Data.Division.AddWeapon(_weapon);
                            this.Close();
                        }
                    }
                    break;
                case "Knife":
                    {
                        if ((textBoxName.Text == "") || (textBoxPrice.Text == "") || (textBoxWeight.Text) == "")
                        {
                            MessageBox.Show("All fields must be filled!");
                        }
                        else
                        {
                            double price = Convert.ToDouble(textBoxPrice.Text);
                            double weight = Convert.ToDouble(textBoxWeight.Text);
                            int blade = Convert.ToInt32(textBoxBlade.Text);
                            _weapon = new Knife(textBoxName.Text,blade,price,weight);
                            Data.Division.AddWeapon(_weapon);
                            this.Close();
                        }
                    }
                    break;
                case "Rifle":
                    {
                        if ((textBoxName.Text == "") || (textBoxWeight.Text == "") || (textBoxCalibre.Text == "") || (textBoxCapacity.Text == "")
                             || (textBoxPrice.Text == "") || (textBoxRange.Text == ""))
                        {
                            MessageBox.Show("All fields must be filled!");
                        }
                        else
                        {
                            double price = Convert.ToDouble(textBoxPrice.Text);
                            double weight = Convert.ToDouble(textBoxWeight.Text);
                            int capacity = Convert.ToInt32(textBoxCapacity.Text);
                            double calibre = Convert.ToDouble(textBoxCalibre.Text);
                            int range = Convert.ToInt32(textBoxRange.Text);
                            int zoom = Convert.ToInt32(comboBoxZoom.SelectedItem);
                            _weapon = new Rifle(textBoxName.Text, price,weight,range,zoom,capacity,calibre);
                            Data.Division.AddWeapon(_weapon);
                            this.Close();
                        }
                    }
                    break;
            }
            
            
        }

        private void comboBoxClass_SelectedValueChanged(object sender, EventArgs e)
        {
            SetVisibleAllTxtBox();
            string value = comboBoxClass.SelectedItem.ToString();

            switch (value)
            {
                case "Gun":
                    {
                        textBoxRange.Visible = false;
                        comboBoxZoom.Visible = false;
                        textBoxBlade.Visible = false;
                        textBoxRate.Visible = false;
                        okBtn.Enabled = true;
                    }
                    break;

                case "AssaultRifle":
                    {
                        comboBoxZoom.Visible = false;
                        textBoxBlade.Visible = false;
                        textBoxRange.Visible = false;
                        okBtn.Enabled = true;
                    } break;
                case "Knife":
                    {
                        textBoxCalibre.Visible = false;
                        textBoxCapacity.Visible = false;
                        textBoxRange.Visible = false;
                        comboBoxZoom.Visible = false;
                        comboBoxZoom.Visible = false;
                        textBoxRate.Visible = false;
                        okBtn.Enabled = true;
                    } break;
                case "Rifle":
                    {
                        textBoxBlade.Visible = false;
                        textBoxRate.Visible = false;
                        comboBoxZoom.SelectedItem = "1";
                        okBtn.Enabled = true;
                    }
                    break;
            }
        }

        private void SetVisibleAllTxtBox()
        {
            textBoxPrice.Visible = true;
            textBoxName.Visible = true;
            textBoxWeight.Visible = true;
            textBoxCalibre.Visible = true;
            textBoxCapacity.Visible = true;
            textBoxRange.Visible = true;
            comboBoxZoom.Visible = true;
            textBoxBlade.Visible = true;
            textBoxRate.Visible = true;
        }

        private void SetInvisibleAllTxtBox()
        {
            textBoxPrice.Visible = false;
            textBoxCalibre.Visible = false;
            textBoxName.Visible = false;
            textBoxWeight.Visible = false;
            textBoxCapacity.Visible = false;
            textBoxRange.Visible = false;
            comboBoxZoom.Visible = false;
            textBoxBlade.Visible = false;
            textBoxRate.Visible = false;
        }

        private void AddForm_Load(object sender, EventArgs e)
        {
        }

        private void textBoxCalibre_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Если это не цифра.
            if (!Char.IsDigit(e.KeyChar))
            {
                // Запрет на ввод более одной десятичной точки.
                if (e.KeyChar != (char)Keys.Back)  
                {
                    if (e.KeyChar != ',' || textBoxCalibre.Text.IndexOf(",") != -1)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBoxCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxRange_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxWeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Если это не цифра.
            if (!Char.IsDigit(e.KeyChar))
            {
                // Запрет на ввод более одной десятичной точки.
                if (e.KeyChar != (char)Keys.Back)
                {
                    if (e.KeyChar != ',' || textBoxWeight.Text.IndexOf(",") != -1)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Если это не цифра.
            if (!Char.IsDigit(e.KeyChar))
            {
                // Запрет на ввод более одной десятичной точки.
                if (e.KeyChar != (char)Keys.Back)
                {
                    if (e.KeyChar != ',' || textBoxPrice.Text.IndexOf(",") != -1)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBoxBlade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            if (_weapon.GetType() == typeof(Knife))
            {
                Knife weapon = (Knife)_weapon;
                weapon.Name = textBoxName.Text;
                weapon.Price = Convert.ToDouble(textBoxPrice.Text);
                weapon.Weight = Convert.ToDouble(textBoxWeight.Text);
            }
            if (_weapon.GetType() == typeof(Gun))
            {
                Gun weapon = (Gun)_weapon;
                weapon.Name = textBoxName.Text;
                weapon.Price = Convert.ToDouble(textBoxPrice.Text);
                weapon.Weight = Convert.ToDouble(textBoxWeight.Text);
                weapon.Capacity= Convert.ToInt32(textBoxCapacity.Text);
                weapon.Calibre= Convert.ToDouble(textBoxCalibre.Text);
            }
            if (_weapon.GetType() == typeof(AssaultRifle))
            {
                AssaultRifle weapon = (AssaultRifle)_weapon;

                weapon.Name = textBoxName.Text;
                weapon.Price = Convert.ToDouble(textBoxPrice.Text);
                weapon.Weight = Convert.ToDouble(textBoxWeight.Text);
                weapon.Capacity = Convert.ToInt32(textBoxCapacity.Text);
                weapon.Calibre = Convert.ToDouble(textBoxCalibre.Text);
                weapon.Rate = Convert.ToInt32(textBoxRate.Text);

            }
            if (_weapon.GetType() == typeof(Rifle))
            {
                Rifle weapon = (Rifle)_weapon;
                weapon.Name = textBoxName.Text;
                weapon.Price = Convert.ToDouble(textBoxPrice.Text);
                weapon.Weight = Convert.ToDouble(textBoxWeight.Text);
                weapon.Capacity = Convert.ToInt32(textBoxCapacity.Text);
                weapon.Calibre = Convert.ToDouble(textBoxCalibre.Text);
                weapon.Zoom = Convert.ToInt32(comboBoxZoom.SelectedItem);

            }
            this.Close();
        }
    }
}
