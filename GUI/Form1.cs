﻿using System;
using System.Windows.Forms;
using Entities;
using Logic;

namespace GUI
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
             UpdateData();

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            AddForm addFrm = new AddForm();
            addFrm.Owner = this;
            addFrm.FormClosed += (obj, arg) =>
            {
                UpdateData();
            };
            addFrm.ShowDialog();
            
        }

        private void UpdateData()
        {
            weaponsListBox.DataSource = null;
            weaponsListBox.DataSource = Data.Weapons;
            weaponsListBox.DisplayMember = "Name";
            labelTotalPrice.Text = string.Format("Total price = {0}", DivisionCalculator.GetTotalPrice(Data.Division).ToString());
            labelTotalWeight.Text = string.Format("Total weight = {0}", DivisionCalculator.GetTotalWeight(Data.Division).ToString());
            this.Refresh();
        }   

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            Data.Weapons.Remove((AbstractWeapon)weaponsListBox.SelectedItem);
            UpdateData();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
              AddForm addFrm = new AddForm((AbstractWeapon)weaponsListBox.SelectedItem);
              addFrm.Owner = this;
              addFrm.FormClosed += (obj, arg) =>
              {
                  UpdateData();
              };
              addFrm.ShowDialog();
        }
    }
}
