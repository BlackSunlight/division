﻿using System.Collections.Generic;
using Entities;
using Logic;

namespace GUI
{
    public class Data
    {
        private static Division _division  = DivisionFactory.CreateDivision();
        private static List<AbstractWeapon> _weapons = _division.Weapons;
        public static Division Division
        {
            get { return _division; }
            set { _division = value; }
        }

        public static List<AbstractWeapon> Weapons
        {
            get { return _weapons; }
            set { _weapons = value; }
        }
    }
}
