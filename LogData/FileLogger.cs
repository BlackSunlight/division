﻿using System;
using System.IO;

namespace LogData
{
    public sealed class FileLogger : ILogger
    {
        private static readonly FileLogger _instance = new FileLogger();
        private FileLogger() { }
        public static FileLogger GetInstance
        {
            get { return _instance; }
        }

        public void Log(string message, string fileName)
        {
            string msg = string.Format("{0} {1}", DateTime.Now.ToString(), message);
            var sw = new StreamWriter(fileName, true);
            sw.WriteLine(msg);
            sw.Close();
        }

        public void Log(string message) 
        {
            string msg = string.Format("{0} {1}", DateTime.Now.ToString(), message);
            var sw = new StreamWriter("log.txt",true);
            sw.WriteLine(msg);
            sw.Close();
        }
    }
}
