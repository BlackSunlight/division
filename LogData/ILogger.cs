﻿namespace LogData
{
    public interface ILogger
    {
        /// <summary>
        /// Logs messages in console
        /// </summary>
        /// <param name="message">A message to be written to console</param>
        void Log(string message);

        /// <summary>
        /// Writes log messege in Log file.
        /// </summary>
        /// <param name="message">A message to be written to log file</param>
        /// <param name="fileName">The name of the log file</param>
        void Log(string message, string fileName);
    }
}
