﻿namespace LogData
{
    public static class LoggerFactory
    {
        /// <summary>
        /// Creates an object in dependence on a parameter "type".
        /// </summary>
        /// <param name="type">Specifies the type of instance to be created.</param>
        /// <returns></returns>
        public static ILogger GetLogger(LogType type)
        {
            if (type == LogType.File)
            {
                var log = FileLogger.GetInstance;
                return log;
            }
            else 
            {
                var logger = ConsoleLogger.GetInstance;
                return logger;
            }
            
        }
    }
}
