﻿using System;

namespace LogData
{
    public sealed class ConsoleLogger : ILogger
    {
        private static readonly ConsoleLogger _instance = new ConsoleLogger();

        private ConsoleLogger() { }

        public static ConsoleLogger GetInstance
        {
            get { return _instance; }
        }
        
        public void Log (string message)
        {
            Console.WriteLine("{0} {1}\n",DateTime.Now.ToString(), message);
        }

        public void Log(string message, string fileName) 
        {
            //Забавная бадяга получилась, но без перегрузки метода Log 
            //в интерфейсе синглтон не работает как надо, поэтому пришлось
            //пилить в логгер консоли перегрузку с именем файла =\
            Console.WriteLine("{0} {1}\n", DateTime.Now.ToString(), message);
        }
    }
}
