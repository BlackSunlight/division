﻿using System;
using System.Collections.Generic;
using Entities;

namespace DivisionConsoleApp
{
    public static class InfoPrinter
    {
        public static void ShowInfo(Division division)
        {
            List<AbstractWeapon>  weapons = division.Weapons;
            foreach (var weapon in weapons)
            {
                Console.WriteLine("Name: {0}, Price: {1}, Weight: {2} \n", weapon.Name, weapon.Price, weapon.Weight);
            }
        }
    }
}
