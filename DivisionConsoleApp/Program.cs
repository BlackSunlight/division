﻿using System;
using Entities;
using Logic;

namespace DivisionConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var division = DivisionFactory.CreateDivision();
            InfoPrinter.ShowInfo(division);

            double totalPrice = DivisionCalculator.GetTotalPrice(division);
            Console.WriteLine("Total price = {0}\n", totalPrice);

            double totalWeight = DivisionCalculator.GetTotalWeight(division);
            Console.WriteLine("Total weight = {0}\n", totalWeight);

            Console.ReadKey();
        }
    }
}
